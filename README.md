## Games:  
#### GRAViTY (director, designer, sound engineer)  
- physics-based puzzler  
    - sand-box elements allow for free player experimentation  
- designed to explore non-text communication  
    - “how can we teach the player how to play the game without words?”  
    - “how can we use game elements and assets that intuitively convey information?”  
- won “Best Digital Gameplay” at AIMS Games Festival 2015  

#### ParadigmQuest (gameplay designer)  
- educational card-based game  
- designed to teach theories and analysis on the relationship between games and learning  
- possibility of using the model and structure for paradigm-based analysis in any field  
- prototyped by hand, hence the in-depth components overview  

#### Tyto_Card_Game (solo)  
- educational card game  
    - players try to build a healthy ecosystem via mechanized energy flow  
- an exercise attempting to port a video game (Tyto Ecology) to an analog game  
    - obviously an extreme reduction, but trying to get at the essence of Tyto Ecology (learning what makes for a healthy, stable biome)  
    - uses the Himalayan biome as a demo, but easily expands into all species in Tyto Ecology  

## Other design:  
#### 3D_Statue  
- short video demo of a 3D modeling exercise in Maya  
    - modeling  
    - texturing  
    - lighting  
- based off statue from “The World’s End” movie  

#### Bioshock  
- poster-format analysis of how Bioshock offers political critique through setting  

#### CapitalSim  
- poster-format design work for an educational game teaching political-economy  

#### DIY_Dining  
- poster-format presentation of a designed solution to on-campus dining issues  

#### Ikelos  
- advertisement for the second production of a play I wrote and directed  

#### Polk  
- poster-style presentation of a branding exercise  
    - build a brand around a former U.S. President  

#### Voices  
- website redesign [much needed](http://lakotaeastvoices.com/)  
    - sound warning: the linked page may auto-play music on certain pages  